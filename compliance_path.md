## 3rdParty compliance with KPMG CAB

This process is an illustration of what **should** be the compliance process with Gaia.
**This supposes a CAB being onbard to a Gaia-X federation, which is not the case so far.**

Here, KPMG is considered to be the onboarded CAB for SecNumCloud ComplianceReference.

### Step 1 - SETUP of Federation level objects.
[CRM-1]ComplianceReferenceManager
 Name: ABC-FEDERATION

[CR-1]ComplianceReference
 Name: SecNumCloud
 Issuer: ABC-FEDERATION

[CCS-1]3rdPartyComplianceScheme
CompRef -> SecNumCloud
CABs -> [KPMG, ABC-FEDERATION]

### Step 2 - KPMG assessment and create a Claim 
- "KPMG CAB emits a 3rdPArtyCertificateClaim after assessment process"
- Assessment process: audit the dufour company. Then create a 3rdPArtyCertificateClaim VCexternal to the federation

[3rdCC-1]3rdPArtyCertificateClaim
Issuer: **KPMG**
LocatedServiceOffering: Dufour/SVC1/Loc1
CAB: [KPMG]
ComplianceCertificateScheme: [CCS-1]

### Step 3 Notarization Service -> validate the certification chain for the 3rdPArtyCertificateClaim (CR, CRScheme, Claim) and returns a Credentials
VP [3rdPArtyCertificateClaim / [CCS-1]3rdPartyComplianceScheme / CAB]
3rdPartyCertificateCredentials
Issuer: **FEDERATION**
IsValid: True


## 3rdParty compliance with a CAB not onboarded

This process is an illustration of what is **today** the compliance process with Gaia.
The CAB which performs the assessment is not on board to the eco-system.
Only ABC-FEDERATION is considered as a valid CAB.

Here, KPMG is not and onboarded CAB but the notarization service trusts KPMG for SecNumCloud ComplianceReference.


### Step 1
[CCS-1]3rdPartyComplianceScheme
CompRef -> SecNumCloud
CABs -> [KPMG, ABC-FEDERATION]

### Step 2
- ABC-FEDERATION receives a PDF from KPMG, then emits a 3rdPArtyCertificateClaim after assessment process
- Assessment process: Validate the PDF from a trusted CAB, external to the federation

3rdPartyCertificateClaim
Issuer: **ABC-FEDERATION**
LocatedServiceOffering: Dufour/SVC1/Loc1
CAB: [ABC-FEDERATION]
ComplianceCertificateScheme: [CCS-1]

### Step 3 Notarization Service -> validate the certification chain for the 3rdPArtyCertificateClaim (CR, CRScheme, Claim) and returns a Credentials
VP [3rdPArtyCertificateClaim / [CCS-1]3rdPartyComplianceScheme / CAB]
3rdPartyCertificateCredentials
Issuer: **FEDERATION**
IsValid: True


## Self-Declared compliance

For some compliance reference (ex: CISPE CoC), an eco-system allows self-declared compliance.

### Step 1
[CCS-2]ComplianceScheme
CompRef -> CISPE CoC

### Step 2
- Dufour self emits a CertificateClaim

CertificateClaim
Issuer: Dufour
LocatedServiceOffering: Dufour/SVC1/Loc1
CAB: [Dufour]
ComplianceCertificateScheme: [CCS-2]

### Step 3 Notarization Service -> valid the CertificateClaim and returns a Credentials
VP [CertificateClaim / [CCS-2]ComplianceScheme]

CertificateCredentials
Issuer: FEDERATION -> controls the scheme is valid.
IsValid: True
