# Procédure pour ajouter une licence Apache 2.0 à un repertoire Gitlab

## 1. Ajouter le fichier de licence

Sur la page web du repertoire, cliquez sur le bouton 'Add LICENSE'.

![Add LICENSE](./img/add-license.png)

Entrez 'Apache 2.0' dans la liste des templates, et choisisez le template adéquat.

![select LICENSE](./img/select-license.png)

## 2. Ajouter un badge Apache 2.0

Allez dans les 'Settings / General' du répertoire.

![add badge](./img/badge.png)

Cliquez sur 'Expand badges'.

Saisissez les informations de bagde suivantes:

* Name: Apache 2.0
* Link: https://opensource.org/licenses/Apache-2.0
* Badge image URL: https://img.shields.io/badge/License-Apache_2.0-blue.svg

Enfin, cliquez sur le bouton 'Add badge'

![apache-2.0 badge](./img/apache-2.0-badge.png)
